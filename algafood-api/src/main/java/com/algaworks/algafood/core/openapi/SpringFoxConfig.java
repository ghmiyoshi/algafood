package com.algaworks.algafood.core.openapi;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLStreamHandler;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.algaworks.algafood.api.exception.ApiError;
import com.algaworks.algafood.api.v1.model.model.CozinhaModel;
import com.algaworks.algafood.api.v1.model.model.PedidoResumoModel;
import com.algaworks.algafood.api.v1.openapi.model.CozinhasModelOpenApi;
import com.algaworks.algafood.api.v1.openapi.model.PageableModelOpenApi;
import com.algaworks.algafood.api.v1.openapi.model.PedidosModelOpenApi;
import com.algaworks.algafood.api.v2.model.CozinhaV2Model;
import com.fasterxml.classmate.TypeResolver;

import io.swagger.models.auth.In;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2 // Habilito o suporte ao Swagger
@Import(BeanValidatorPluginsConfiguration.class)
public class SpringFoxConfig implements WebMvcConfigurer{
	
	@Bean
	public Docket apiDocket() {
		var typeResolver = new TypeResolver();
		
		return new Docket(DocumentationType.SWAGGER_2) // Cria o Docket para Swagger 2
				.groupName("v1")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.algaworks.algafood")) // Escanea tudo que está nesse pacote
				.paths(PathSelectors.ant("/v1/**")) // Paths que começam com /v1
				//.paths(PathSelectors.ant("/restaurantes/*"))
				.build()
				.additionalModels(typeResolver.resolve(ApiError.class))
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, getGlobalGetResponseMessages())
				.globalResponseMessage(RequestMethod.POST, getGlobalPostResponseMessages())
				.globalResponseMessage(RequestMethod.PUT, getGlobalPutResponseMessages())
				.globalResponseMessage(RequestMethod.DELETE, getGlobalDeleteResponseMessages())
				//.globalOperationParameters(Arrays.asList( Configurado de forma global
						//new ParameterBuilder()
							//.name("campos")
							//.description("Nomes das propriedades para filtrar na resposta (separar por virgula)")
							//.parameterType("query")
							//.modelRef(new ModelRef("string"))
							//.build()))
				.securitySchemes(Arrays.asList(securityScheme(), new ApiKey("Token Access", HttpHeaders.AUTHORIZATION, In.HEADER.name()))) // Esquema de segurança OAuth
				.securityContexts(Arrays.asList(securityContext())) // Definir quais endpoints serão trancados para testar com try it out
				.apiInfo(apiInfo())
				.directModelSubstitute(Pageable.class, PageableModelOpenApi.class)
				.alternateTypeRules(AlternateTypeRules.newRule(
						typeResolver.resolve(Page.class, CozinhaModel.class), 
						CozinhasModelOpenApi.class)) // Altera Page<CozinhaModel> para CozinhaModelOpenApi
				.alternateTypeRules(AlternateTypeRules.newRule(
	                    typeResolver.resolve(Page.class, PedidoResumoModel.class),
	                    PedidosModelOpenApi.class))
				.ignoredParameterTypes(ServletWebRequest.class, 
									   URL.class, 
									   URI.class, 
									   URLStreamHandler.class,
									   Resource.class,
									   File.class,
									   InputStream.class,
									   MappingJacksonValue.class)
				.tags(tags()[0], tags()); 
	}
	
	private SecurityScheme securityScheme() { // Esquema de segurança OAuth
		return new OAuthBuilder()
					.name("Algafood")
					.grantTypes(grantTypes()) // Grant types disponiveis para ser usado na documentação Swagger
					.scopes(scopes()).build(); // Lista de escopos disponiveis
	}
	
	private SecurityContext securityContext() {
		var securityReferenceOAuth = SecurityReference.builder()
								.reference("Algafood")
								.scopes(scopes().toArray(new AuthorizationScope[0])) // Converto uma lista para um array de AuthorizationScope
								.build();
		
		var securityReferenceTokenAccess = SecurityReference.builder()
								.reference("Token Access")
								.scopes(scopes().toArray(new AuthorizationScope[0]))
								.build();
		
		return SecurityContext.builder()
					.securityReferences(Arrays.asList(securityReferenceOAuth, securityReferenceTokenAccess))
					.forPaths(PathSelectors.any())
					.build();
	}
	
	
	private List<GrantType> grantTypes() {
		return Arrays.asList(new ResourceOwnerPasswordCredentialsGrant("/oauth/token")); // Grant type Password flow
	}
	
	private List<AuthorizationScope> scopes() {
		return Arrays.asList(new AuthorizationScope("READ", "Acesso de leitura"), 
							 new AuthorizationScope("WRITE", "Acesso de escrita"));
	}
	
	@Bean
	public Docket apiV2Docket() {
		var typeResolver = new TypeResolver();
		
		return new Docket(DocumentationType.SWAGGER_2) // Cria o Docket para Swagger 2
				.groupName("v2")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.algaworks.algafood")) // Escanea tudo que está nesse pacote
				.paths(PathSelectors.ant("/v2/**")) // Paths que começam com /v2
				//.paths(PathSelectors.ant("/restaurantes/*"))
				.build()
				.additionalModels(typeResolver.resolve(ApiError.class))
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, getGlobalGetResponseMessages())
				.globalResponseMessage(RequestMethod.POST, getGlobalPostResponseMessages())
				.globalResponseMessage(RequestMethod.PUT, getGlobalPutResponseMessages())
				.globalResponseMessage(RequestMethod.DELETE, getGlobalDeleteResponseMessages())
				//.globalOperationParameters(Arrays.asList( Configurado de forma global
						//new ParameterBuilder()
							//.name("campos")
							//.description("Nomes das propriedades para filtrar na resposta (separar por virgula)")
							//.parameterType("query")
							//.modelRef(new ModelRef("string"))
							//.build()))
				.apiInfo(apiV2Info())
				.directModelSubstitute(Pageable.class, PageableModelOpenApi.class)
				.alternateTypeRules(AlternateTypeRules.newRule(
						typeResolver.resolve(Page.class, CozinhaV2Model.class), 
						CozinhasModelOpenApi.class))
				.ignoredParameterTypes(ServletWebRequest.class, 
									   URL.class, 
									   URI.class, 
									   URLStreamHandler.class,
									   Resource.class,
									   File.class,
									   InputStream.class,
									   MappingJacksonValue.class)
				.tags(tagsV2()[0], tagsV2()); 
	}
	
	private List<ResponseMessage> getGlobalGetResponseMessages() {
	    return Arrays.asList(
	    	// TO DO ajustar HttpStatus.INTERNAL_SERVER_ERROR. Retorna somente para os endpoints de buscar e não listar
	        responseMessage(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error", "Erro"),
	        responseMessage(HttpStatus.NOT_ACCEPTABLE, "Not acceptable")
	    );
	}

	private List<ResponseMessage> getGlobalPostResponseMessages() {
	    return Arrays.asList(
	        responseMessage(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error", "Erro"),
	        responseMessage(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"),
	        responseMessage(HttpStatus.NOT_ACCEPTABLE, "Not acceptable"),
	        responseMessage(HttpStatus.BAD_REQUEST, "Bad request", "Erro")
	    );
	}

	private List<ResponseMessage> getGlobalPutResponseMessages() {
	    return Arrays.asList(
	        responseMessage(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error", "Erro"),
	        responseMessage(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"),
	        responseMessage(HttpStatus.NOT_FOUND, "Not Found"),
	        responseMessage(HttpStatus.NOT_ACCEPTABLE, "Not acceptable"),
	        responseMessage(HttpStatus.BAD_REQUEST, "Bad request", "Erro")
	    );
	}

	private List<ResponseMessage> getGlobalDeleteResponseMessages() {
	    return Arrays.asList(
	        responseMessage(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error", "Erro"),
	        responseMessage(HttpStatus.NOT_FOUND, "Not Found"),
	        responseMessage(HttpStatus.BAD_REQUEST, "Bad request", "Erro")
	    );
	}

	private ResponseMessage responseMessage(HttpStatus status, String message) {
	    return new ResponseMessageBuilder()
	            .code(status.value())
	            .message(message)
	            .build();
	}
	
	private ResponseMessage responseMessage(HttpStatus status, String message, String type) {
	    return new ResponseMessageBuilder()
	            .code(status.value())
	            .message(message)
	            .responseModel(new ModelRef(type))
	            .build();
	}
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Algafood API")
				.version("1.0")
				.description("API pública para clientes e restaurantes. <br>" +
						"<strong>Essa versão da API está depreciada e deixará de existir a partir de 01/01/2021. Use a versão mais atual da API.</strong>")
				.contact(new Contact("Algaworks", "https://algaworks.com.br", "contato@algaworks.com"))
				.build();
	}
	
	private ApiInfo apiV2Info() {
		return new ApiInfoBuilder()
				.title("Algafood API")
				.version("2.0")
				.description("API pública para clientes e restaurantes")
				.contact(new Contact("Algaworks", "https://algaworks.com.br", "contato@algaworks.com"))
				.build();
	}
	
	private Tag[] tags() {
		return new Tag[] {
			new Tag("Cidades", "Gerencia as cidades"),
			new Tag("Cozinhas", "Gerencia as cozinhas"),
			new Tag("Grupos", "Gerencia os grupos de usuários"),
			new Tag("Formas de Pagamento", "Gerencia as formas de pagamento"),
			new Tag("Pedidos", "Gerencia os pedidos"),
			new Tag("Restaurantes", "Gerencia os restaurantes"),
			new Tag("Estados", "Gerencia os estados"),
			new Tag("Produtos", "Gerencia os produtos de restaurantes"),
			new Tag("Usuários", "Gerencia os usuários")
		}; 
	}
	
	private Tag[] tagsV2() {
		return new Tag[] {
			new Tag("Cidades", "Gerencia as cidades"),
			new Tag("Cozinhas", "Gerencia as cozinhas")
		}; 
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) { // Método para gerar o html do Swagger
		registry.addResourceHandler("swagger-ui.html") // Permitir acesso a esse html
				.addResourceLocations("classpath:/META-INF/resources/"); // Caminho do html
		
		registry.addResourceHandler("/webjars/**") // Permitir acesso a arquivos que estão na pasta /webjars e qualquer coisa em diante 
				.addResourceLocations("classpath:/META-INF/resources/webjars/"); // Caminho do html
	}

}
