package com.algaworks.algafood.core.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class ApiDeprecationHandler extends HandlerInterceptorAdapter {
	
	/**
	 *  Intercepta as requisições que começam com /v1 e adiciona no header X-AlgaFood-Deprecated com a mensagem definida
	 *  Para funcionar criei no WebConfig o método addInterceptors, que registra esse interceptador.
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if(request.getRequestURI().startsWith("/v1/")) {
			//response.addHeader(
				//	"X-AlgaFood-Deprecated", 
					//"Essa versão da API está depreciada e deixará de existir a partir de 01/01/2021");
			response.setStatus(HttpStatus.GONE.value());
			return true;
		}
		
		// true para não interromper a execução do método
		return true;
	}

}
