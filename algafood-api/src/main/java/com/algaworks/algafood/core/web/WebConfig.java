package com.algaworks.algafood.core.web;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.ShallowEtagHeaderFilter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	
	@Autowired
	private ApiDeprecationHandler apiDeprecationHandler;
	
	// Configuração para habilitar o CORS globalmente, ou seja, para todos os endpoints
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				  .allowedMethods("*"); // GET, POST, PUT, DELETE, TODOS
				//.allowedOrigins"*") // Esse é o padrão, aceita qualquer coisa
				//.maxAge(30) // Armazena no cache por 30 seg (preflight)
	}
	
	// Configuração para definir o content type padrão
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.defaultContentType(AlgaFoodMediaTypes.V2_APPLICATION_JSON);
	}
	
	// Configuração para adicionar no header ETag
	@Bean
	public Filter shallowEtagHeaderFilter() {
		return new ShallowEtagHeaderFilter();
	}
	
	// Configuração para registrar o interceptador
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(apiDeprecationHandler);
	}

}
