package com.algaworks.algafood.core.validation;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.web.multipart.MultipartFile;

public class FileContentTypeValidator implements ConstraintValidator<FileContentType, MultipartFile> {

	private List<String> allowedContentTypes;

	@Override
	public void initialize(FileContentType fileContentType) {
		this.allowedContentTypes = Arrays.asList(fileContentType.allowed());
	}

	@Override
	public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
		boolean valido = false;

		if (allowedContentTypes.contains(value.getContentType()) || value.getContentType() == null) {
			valido = true;
		}

		return valido;
	}

}
