package com.algaworks.algafood.core.modelmapper;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.algaworks.algafood.api.v1.model.input.ItemPedidoInput;
import com.algaworks.algafood.api.v1.model.model.EnderecoModel;
import com.algaworks.algafood.api.v2.model.input.CidadeV2Input;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Endereco;
import com.algaworks.algafood.domain.model.ItemPedido;

@Configuration
public class ModelMapperConfig {

	@Bean
	public ModelMapper modelMapper() {
		var modelMapper = new ModelMapper();
		
//		modelMapper.createTypeMap(Restaurante.class, RestauranteModel.class)
//				   .addMapping(Restaurante::getTaxaFrete, RestauranteModel::setPrecoFrete);
		
		// Resolve problema que está atribuindo o idEstado para o id da Cidade. Sem isso, ao tentar adicionar uma cidade executa um update
		modelMapper.createTypeMap(CidadeV2Input.class, Cidade.class)
	    .addMappings(mapper -> mapper.skip(Cidade::setId));  
		
		// Ignoro o mapeamento para o setId() Não atribuo o produtoId do ItemPedidoInput para o id do ItemPedido (Sem essa configuração da erro detached ItemPedido)
		modelMapper.createTypeMap(ItemPedidoInput.class, ItemPedido.class)
	    .addMappings(mapper -> mapper.skip(ItemPedido::setId));  
//		
		var enderecoToEnderecoModelTypeMap = modelMapper.createTypeMap(Endereco.class, EnderecoModel.class);
		
		enderecoToEnderecoModelTypeMap.<String>addMapping(
				enderecoSrc -> enderecoSrc.getCidade().getEstado().getNome(), //source getter
				(enderecoModelDest, value) -> enderecoModelDest.getCidade().setEstado(value)); //destination setter
		
//		modelMapper.createTypeMap(Restaurante.class, RestauranteModel.class)
//        .addMapping(Restaurante::getTaxaFrete, RestauranteModel::setPrecoFrete)
//        .addMapping(restSrc -> restSrc.getEndereco().getCidade().getEstado().getNome(), 
//        	(restDest, val) -> restDest.getEndereco().getCidade().setEstado((String) val));
		
		return modelMapper;
	}

}
