package com.algaworks.algafood.domain.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import com.algaworks.algafood.domain.event.PedidoConfirmadoEvent;
import com.algaworks.algafood.domain.service.EnvioEmailService;
import com.algaworks.algafood.domain.service.EnvioEmailService.Mensagem;

@Component
public class NotificacaoClientePedidoConfirmadoListener {
	
	@Autowired
	private EnvioEmailService envioEmailService;
	
	@TransactionalEventListener // (phase = TransactionPhase.BEFORE_COMMIT) Está como AFTER, caso dê um erro será executado o update mas não será enviado o email
	public void aoConfirmarPedido(PedidoConfirmadoEvent event) {
		var mensagem = Mensagem.builder()
				.assunto(event.getPedido().getRestaurante().getNome() + " - Pedido Confirmado")
				.corpo("pedido-confirmado.html") // Template que será usado pelo Apache Freemarker
				.variavel("pedido", event.getPedido()) // Passo o objeto pedido para customizar o template usando seus atributos
				.destinatario(event.getPedido().getCliente().getEmail())
				.build();
		
		envioEmailService.enviar(mensagem);
	}

}
