package com.algaworks.algafood.domain.model;

import java.util.Arrays;
import java.util.List;

public enum StatusPedido {

	CRIADO("Criado"), CONFIRMADO("Confirmado", CRIADO), ENTREGUE("Entregue", CONFIRMADO), CANCELADO("Cancelado", CRIADO, CONFIRMADO);

	private String descricao;
	private List<StatusPedido> statusAnteriores;

	private StatusPedido(String descricao, StatusPedido... statusAnteriores) {
		this.descricao = descricao;
		this.statusAnteriores = Arrays.asList(statusAnteriores);
	}

	public String getDescricao() {
		return descricao;
	}

	public List<StatusPedido> getStatusAnteriores() {
		return statusAnteriores;
	}
	
	public boolean naoPodeAlterarPara(StatusPedido novoStatus) { // Status Pedido Confirmado
		System.out.println(this); // Status Pedido Criado
		return !novoStatus.getStatusAnteriores().contains(this);
	}

}
