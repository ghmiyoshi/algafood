package com.algaworks.algafood.domain.exception;

public class PermissaoNaoEncontradoException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public PermissaoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}

	public PermissaoNaoEncontradoException(Long grupoId) {
		this(String.format("Não existe cadastro de permissão com código %d", grupoId));
	}

}
