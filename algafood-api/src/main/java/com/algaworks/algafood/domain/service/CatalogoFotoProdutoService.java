package com.algaworks.algafood.domain.service;

import java.io.InputStream;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.algaworks.algafood.domain.exception.FotoProdutoNaoEncontradaException;
import com.algaworks.algafood.domain.model.FotoProduto;
import com.algaworks.algafood.domain.repository.ProdutoRepository;
import com.algaworks.algafood.domain.service.FotoStorageService.NovaFoto;

@Service
public class CatalogoFotoProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private FotoStorageService fotoStorageService;

	@Transactional
	public FotoProduto salvar(FotoProduto fotoProduto, InputStream dadosArquivo) {
		Optional<FotoProduto> fotoEncontrada = produtoRepository.findFotoById(fotoProduto.getRestauranteId(), fotoProduto.getProduto().getId());
		String nomeNovoArquivo = fotoStorageService.gerarNomeArquivo(fotoProduto.getNomeArquivo());
		String nomeArquivoExistente = null;
		
		
		if (fotoEncontrada.isPresent()) {
			nomeArquivoExistente = fotoEncontrada.get().getNomeArquivo();
			produtoRepository.excluirFoto(fotoEncontrada.get());
		}
		
		fotoProduto.setNomeArquivo(nomeNovoArquivo);
		fotoProduto = produtoRepository.save(fotoProduto);
		produtoRepository.flush();
		
		NovaFoto novaFoto = NovaFoto.builder()
							.nomeArquivo(fotoProduto.getNomeArquivo())
							.contentType(fotoProduto.getContentType())
							.inputStream(dadosArquivo)
							.build();
		
		fotoStorageService.substituir(nomeArquivoExistente, novaFoto);

		return fotoProduto;
	}

	public FotoProduto buscarOuFalhar(Long restauranteId, Long produtoId) {
		FotoProduto foto = produtoRepository.findFotoById(restauranteId, produtoId)
				.orElseThrow(() -> new FotoProdutoNaoEncontradaException(String.format(
						"Não existe um cadastro de foto do produto com código %d para o restaurante de código %d",
						produtoId, restauranteId)));

		return foto;
	}

	@Transactional
	public void removerFoto(Long restauranteId, Long produtoId) {
		FotoProduto foto = buscarOuFalhar(restauranteId, produtoId);
		
		produtoRepository.excluirFoto(foto);
		produtoRepository.flush();
		
		fotoStorageService.remover(foto.getNomeArquivo());
	}

}
