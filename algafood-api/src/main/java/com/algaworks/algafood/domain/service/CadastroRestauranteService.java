package com.algaworks.algafood.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.exception.RestauranteNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.repository.FormaPagamentoRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.domain.repository.UsuarioRepository;

@Service
public class CadastroRestauranteService {

	@Autowired
	private RestauranteRepository restauranteRepository;

	@Autowired
	private CozinhaRepository cozinhaRepository;

	@Autowired
	private CidadeRepository cidadeRepository;

	@Autowired
	private FormaPagamentoRepository formaPagamentoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Transactional
	public Restaurante salvar(Restaurante restaurante) {
		Long cozinhaId = restaurante.getCozinha().getId();
		Long cidadeId = restaurante.getEndereco().getCidade().getId();

		Cozinha cozinha = cozinhaRepository.buscarOuFalhar(cozinhaId);
		restaurante.setCozinha(cozinha);

		Cidade cidade = cidadeRepository.buscarOuFalhar(cidadeId);
		restaurante.getEndereco().setCidade(cidade);

		return restauranteRepository.save(restaurante);
	}

	@Transactional
	public void remover(Long restauranteId) {
		try {
			restauranteRepository.deleteById(restauranteId);
			restauranteRepository.flush();
		} catch (EmptyResultDataAccessException e) {
			throw new RestauranteNaoEncontradoException(restauranteId);
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
					String.format("Restaurante de código %d não pode ser removida, pois está em uso", restauranteId));
		}
	}

	@Transactional
	public void ativar(Long restauranteId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);
		restaurante.ativar();
	}
	
	@Transactional
	public void ativar(List<Long> restaurantesIds) {
		restaurantesIds.forEach(this::ativar);
	}

	@Transactional
	public void inativar(Long restauranteId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);
		restaurante.inativar();
	}
	
	@Transactional
	public void inativar(List<Long> restaurantesIds) {
		restaurantesIds.forEach(this::inativar);
	}

	@Transactional
	public void desassociarFormaPagamento(Long restauranteId, Long formaPagamentoId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);
		FormaPagamento formaPagamento = formaPagamentoRepository.buscarOuFalhar(formaPagamentoId);

		restaurante.removerFormaPagamento(formaPagamento);
	}

	@Transactional
	public void associarFormaPagamento(Long restauranteId, Long formaPagamentoId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);
		FormaPagamento formaPagamento = formaPagamentoRepository.buscarOuFalhar(formaPagamentoId);

		restaurante.adicionarFormaPagamento(formaPagamento);
	}

	@Transactional
	public void abrir(Long restauranteId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);

		restaurante.abrir();
	}

	@Transactional
	public void fechar(Long restauranteId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);

		restaurante.fechar();
	}

	@Transactional
	public void associarResponsavel(Long restauranteId, Long usuarioId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);
		Usuario usuario = usuarioRepository.buscarOuFalhar(usuarioId);

		restaurante.adicionarResponsavel(usuario);
	}

	@Transactional
	public void desassociarResponsavel(Long restauranteId, Long usuarioId) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);
		Usuario usuario = usuarioRepository.buscarOuFalhar(usuarioId);

		restaurante.removerResponsavel(usuario);
	}

}
