package com.algaworks.algafood.domain.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.algaworks.algafood.domain.model.Restaurante;

@Repository
public interface RestauranteRepository extends CustomJpaRepository<Restaurante, Long>, RestauranteRepositoryQueries, JpaSpecificationExecutor<Restaurante> {

	@Query("from Restaurante r join fetch r.cozinha") // Executo somente um SELECT
	List<Restaurante> findAll();
	
	List<Restaurante> findByTaxaFreteBetween(BigDecimal taxaInicial, BigDecimal taxaFinal); // Query method usando o keyword Between
	
	// @Query("from Restaurante where nome like %:nome% and cozinha.id = :id") Exposto para o arquivo orm.xml em /resources/META-INF
	List<Restaurante> consultaPorNomeNaCozinhaDeId(String nome, @Param("id") Long cozinhaId);
	
	List<Restaurante> findByNomeContainingAndCozinhaId(String nome, Long id); // keyword and
	
	Optional<Restaurante> findFirstRestauranteByNomeContaining(String nome); // keyword first
	
	List<Restaurante> findTop2ByNomeContaining(String nome); // keyword top2 traz os dois primeiros restaurantes
	
	int countByCozinhaId(Long id); // prefixo count
	
	boolean existeResponsavel(Long restauranteId, Long usuarioId); // Query está no arquivo orm.xml
	
}
