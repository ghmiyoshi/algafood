package com.algaworks.algafood.infrastructure.service.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.algaworks.algafood.domain.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.StatusPedido;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;
import com.algaworks.algafood.domain.service.VendaQueryService;

@Repository
public class VendaQueryServiceImpl implements VendaQueryService {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<VendaDiaria> consultarVendasDiarias(VendaDiariaFilter filtro, String timeOffset) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<VendaDiaria> query = builder.createQuery(VendaDiaria.class);
		Root<Pedido> root = query.from(Pedido.class);

		Expression<Date> functionConvertTzDataCriacao = builder.function("convert_tz", Date.class, root.get("dataCriacao"), builder.literal("+00:00"), builder.literal(timeOffset));
		Expression<Date> functionDataCriacao = builder.function("date", Date.class, functionConvertTzDataCriacao);
		
		
		CompoundSelection<VendaDiaria> selection = builder.construct(VendaDiaria.class, // chamo o construtor de VendaDiaria
				functionDataCriacao, // função nativa do sql date
				builder.count(root.get("id")), // count de produto.id
				builder.sum(root.get("valorTotal"))); // sum de produto.valorTotal

		ArrayList<Predicate> predicates = new ArrayList<>(); // crio uma lista de predicates para passar no where do criteria

		if (filtro.getRestauranteId() != null) {
			predicates.add(builder.equal(root.get("restaurante"), filtro.getRestauranteId()));
		}

		if (filtro.getDataCriacaoInicio() != null) {
			predicates.add(builder.greaterThanOrEqualTo(root.get("dataCriacao"), filtro.getDataCriacaoInicio()));
		}

		if (filtro.getDataCriacaoFim() != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get("dataCriacao"), filtro.getDataCriacaoFim()));
		}

		predicates.add(root.get("status").in(StatusPedido.CONFIRMADO, StatusPedido.ENTREGUE));

		/*
		 * SELECT date(p.data_criacao) as data_criacao, count(p.id) as total_vendas,
		 * sum(p.valor_total) as total_faturado 
		 * FROM pedido p GROUP BY
		 * date(p.data_criacao)
		 */

		query.select(selection);
		query.where(predicates.toArray(new Predicate[0]));
		query.groupBy(functionDataCriacao);

		return manager.createQuery(query).getResultList();
	}

}
