package com.algaworks.algafood.infrastructure.service.report;

import java.util.HashMap;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.algaworks.algafood.domain.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.service.VendaQueryService;
import com.algaworks.algafood.domain.service.VendaReportService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class PdfVendaReportServiceImpl implements VendaReportService {

	@Autowired
	private VendaQueryService vendaQueryService;

	@Override
	public byte[] emitirVendasDiarias(VendaDiariaFilter filtro, String timeOffset) {
		try {
			var inputStream = this.getClass().getResourceAsStream("/relatorios/vendas-diarias.jasper"); // Path do arquivo

			var parametros = new HashMap<String, Object>(); 
			parametros.put("REPORT_LOCALE", new Locale("pt", "BR")); // PT BR para exibir no relatório R$

			var vendasDiarias = vendaQueryService.consultarVendasDiarias(filtro, timeOffset);
			var dataSource = new JRBeanCollectionDataSource(vendasDiarias); // Fonte de dados

			var jasperPrint = JasperFillManager.fillReport(inputStream, parametros, dataSource); // jasperPrint representa um objeto de um relatorio preenchido
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException ex) {
			throw new ReportException("Não foi possível emitir relatório de vendas diárias", ex);
		}

	}

}
