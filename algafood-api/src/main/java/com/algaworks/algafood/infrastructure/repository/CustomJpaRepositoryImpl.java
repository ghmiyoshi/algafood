package com.algaworks.algafood.infrastructure.repository;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.algaworks.algafood.domain.exception.EntidadeNaoEncontradaException;
import com.algaworks.algafood.domain.repository.CustomJpaRepository;

// Criando um repositorio generico
public class CustomJpaRepositoryImpl<T, ID> extends SimpleJpaRepository<T, ID> implements CustomJpaRepository<T, ID> {

	private EntityManager manager;

	public CustomJpaRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);
		this.manager = entityManager;
	}

	@Override
	public Optional<T> buscaPrimeiro() {
		var jpql = "from " + getDomainClass().getName();

		T entity = manager.createQuery(jpql, getDomainClass()).setMaxResults(1).getSingleResult();

		return Optional.ofNullable(entity);
	}

	@Override
	public T buscarOuFalhar(Long id) {
		var jpql = "from " + getDomainClass().getName() + " where id = :codigo";

		try {
			T entity = manager.createQuery(jpql, getDomainClass()).setParameter("codigo", id).getSingleResult();

			return entity;
		} catch (NoResultException e) {
			String simpleName = getDomainClass().getSimpleName();
			
			throw new EntidadeNaoEncontradaException(
					String.format("Não existe um cadastro de %s com código %d", simpleName, id));
		}

	}

	@Override
	public void detach(T entity) {
		manager.detach(entity);
	}

}
