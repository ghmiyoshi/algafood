package com.algaworks.algafood.api.v2.openapi.controller;

import java.util.List;

import com.algaworks.algafood.api.exception.ApiError;
import com.algaworks.algafood.api.v2.model.CidadeV2Model;
import com.algaworks.algafood.api.v2.model.input.CidadeV2Input;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Cidades")
public interface CidadeV2ControllerOpenApi {
	
	@ApiOperation("Lista as cidades")
	List<CidadeV2Model> listar();

	@ApiOperation("Busca uma cidade por ID")
	@ApiResponses({
		@ApiResponse(code = 400, message = "ID da cidade inválido", response = ApiError.class),
		@ApiResponse(code = 404, message = "Cidade não encontrada", response = ApiError.class)
	})
	CidadeV2Model buscar(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long id);

	@ApiOperation("Cadastra uma cidade")
	@ApiResponses({
		@ApiResponse(code = 201, message = "Cidade cadastrada")
	})
	CidadeV2Model salvar(@ApiParam(name = "corpo", value = "Representação de uma nova cidade") CidadeV2Input cidadeV2Input);

	@ApiOperation("Exclui uma cidade por ID")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Cidade excluída", response = ApiError.class),
		@ApiResponse(code = 404, message = "Cidade não encontrada", response = ApiError.class)
	})
	void excluir(@ApiParam(value = "ID de uma cidade", example = "1", required = true) Long id);

	@ApiOperation("Atualiza uma cidade por ID")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Cidade atualizada", response = ApiError.class),
		@ApiResponse(code = 404, message = "Cidade não encontrada", response = ApiError.class)
	})
	CidadeV2Model atualizar(
			@ApiParam(value="ID de uma cidade", example = "1", required = true) Long id,
			
			@ApiParam(name = "corpo", value = "Representação de uma cidade com novos dados") CidadeV2Input cidadeV2Input);

}
