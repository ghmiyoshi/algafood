package com.algaworks.algafood.api.v2.openapi.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.algaworks.algafood.api.exception.ApiError;
import com.algaworks.algafood.api.v2.model.CozinhaV2Model;
import com.algaworks.algafood.api.v2.model.input.CozinhaV2Input;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Cozinhas")
public interface CozinhaV2ControllerOpenApi {

	@ApiOperation("Lista as cozinhas com paginação")
	Page<CozinhaV2Model> listar(Pageable pageable);

	@ApiOperation("Busca uma cozinha por ID")
	@ApiResponses({ 
			@ApiResponse(code = 400, message = "ID da cozinha inválido", response = ApiError.class),
			@ApiResponse(code = 404, message = "Cozinha não encontrada", response = ApiError.class) })
	CozinhaV2Model buscar(
			@ApiParam(value = "ID de uma cozinha", example = "1") Long cozinhaId);

	@ApiOperation("Cadastra uma cozinha")
	@ApiResponses({ 
			@ApiResponse(code = 201, message = "Cozinha cadastrada"), })
	CozinhaV2Model adicionar(
			@ApiParam(name = "corpo", value = "Representação de uma nova cozinha") CozinhaV2Input cozinhaV2Input);

	@ApiOperation("Atualiza uma cozinha por ID")
	@ApiResponses({ 
			@ApiResponse(code = 200, message = "Cozinha atualizada"),
			@ApiResponse(code = 404, message = "Cozinha não encontrada", response = ApiError.class) })
	CozinhaV2Model atualizar(
			@ApiParam(value = "ID de uma cozinha", example = "1", required = true) Long cozinhaId,
			@ApiParam(name = "corpo", value = "Representação de uma cozinha com os novos dados") CozinhaV2Input cozinhaV2Input);

	@ApiOperation("Exclui uma cozinha por ID")
	@ApiResponses({ 
			@ApiResponse(code = 204, message = "Cozinha excluída"),
			@ApiResponse(code = 404, message = "Cozinha não encontrada", response = ApiError.class) })
	void remover(
			@ApiParam(value = "ID de uma cozinha", example = "1", required = true) Long cozinhaId);
	
}
