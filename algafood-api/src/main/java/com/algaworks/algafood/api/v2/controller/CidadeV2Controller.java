package com.algaworks.algafood.api.v2.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v2.model.CidadeV2Model;
import com.algaworks.algafood.api.v2.model.input.CidadeV2Input;
import com.algaworks.algafood.api.v2.openapi.controller.CidadeV2ControllerOpenApi;
import com.algaworks.algafood.core.web.AlgaFoodMediaTypes;
import com.algaworks.algafood.domain.exception.EstadoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.service.CadastroCidadeService;

import io.swagger.annotations.Api;

@Api(tags = "Cidades")
@RestController
@RequestMapping(path = "/v2/cidades", produces = AlgaFoodMediaTypes.V2_APPLICATION_JSON_VALUE)
public class CidadeV2Controller implements CidadeV2ControllerOpenApi{

	@Autowired
	private CidadeRepository cidadeRepository;

	@Autowired
	private CadastroCidadeService cidadeService;
	
	@Autowired
	private GenericModelMapper<Cidade, CidadeV2Model> modelMapper;
	
	@Autowired
	private GenericInputMapper<CidadeV2Input, Cidade> inputMapper;

	@GetMapping
	public List<CidadeV2Model> listar() {
		List<CidadeV2Model> cidadesModel = modelMapper.toCollectionModel(cidadeRepository.findAll(), CidadeV2Model.class);
		
		return cidadesModel;
	}

	@GetMapping("/{id}")
	public CidadeV2Model buscar(@PathVariable Long id) {
		return modelMapper.toModel(cidadeRepository.buscarOuFalhar(id), CidadeV2Model.class);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CidadeV2Model salvar(@Valid @RequestBody CidadeV2Input CidadeV2Input) {
		try {
			Cidade cidade = inputMapper.toDomain(CidadeV2Input, Cidade.class);
			cidadeService.salvar(cidade);
			
			return modelMapper.toModel(cidade, CidadeV2Model.class);
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable Long id) {
		cidadeService.remover(id);
	}

	@PutMapping("/{id}")
	public CidadeV2Model atualizar(@PathVariable Long id, @Valid @RequestBody CidadeV2Input CidadeV2Input) {
		try {
			Cidade cidadeAtual = cidadeRepository.buscarOuFalhar(id);
			// BeanUtils.copyProperties(CidadeV2Input, cidadeAtual, "id");

			cidadeAtual.setEstado(new Estado());
			
			inputMapper.copyToDomainObject(CidadeV2Input, cidadeAtual);
			cidadeAtual = cidadeService.salvar(cidadeAtual);

			return modelMapper.toModel(cidadeAtual, CidadeV2Model.class);
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
