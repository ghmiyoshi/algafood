package com.algaworks.algafood.api.v2.openapi.model;

import com.algaworks.algafood.api.v1.openapi.model.PagedModelOpenApi;
import com.algaworks.algafood.api.v2.model.CozinhaV2Model;

import io.swagger.annotations.ApiModel;

@ApiModel("CozinhasV2Model")
public class CozinhasV2ModelOpenApi extends PagedModelOpenApi<CozinhaV2Model> {

}
