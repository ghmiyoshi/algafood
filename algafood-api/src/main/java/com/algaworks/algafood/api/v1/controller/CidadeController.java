package com.algaworks.algafood.api.v1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.CidadeInput;
import com.algaworks.algafood.api.v1.model.model.CidadeModel;
import com.algaworks.algafood.api.v1.openapi.controller.CidadeControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.core.web.AlgaFoodMediaTypes;
import com.algaworks.algafood.domain.exception.EstadoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.service.CadastroCidadeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/v1/cidades", produces = AlgaFoodMediaTypes.V1_APPLICATION_JSON_VALUE)
public class CidadeController implements CidadeControllerOpenApi{

	@Autowired
	private CidadeRepository cidadeRepository;

	@Autowired
	private CadastroCidadeService cidadeService;
	
	@Autowired
	private GenericModelMapper<Cidade, CidadeModel> modelMapper;
	
	@Autowired
	private GenericInputMapper<CidadeInput, Cidade> inputMapper;

	@CheckSecurity.Cidade.PodeConsultar
	@Deprecated
	@GetMapping
	public List<CidadeModel> listar() {
		log.info("[INICIO] Processo: Listar cidades");
		List<CidadeModel> cidadesModel = modelMapper.toCollectionModel(cidadeRepository.findAll(), CidadeModel.class);
		log.info("[FINAL] Processo: Listar cidades");
		return cidadesModel;
	}

	@CheckSecurity.Cidade.PodeConsultar
	@GetMapping("/{id}")
	public CidadeModel buscar(@PathVariable Long id) {
		return modelMapper.toModel(cidadeRepository.buscarOuFalhar(id), CidadeModel.class);
	}

	@CheckSecurity.Cidade.PodeEditar
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CidadeModel salvar(@Valid @RequestBody CidadeInput cidadeInput) {
		try {
			Cidade cidade = inputMapper.toDomain(cidadeInput, Cidade.class);
			cidadeService.salvar(cidade);
			
			return modelMapper.toModel(cidade, CidadeModel.class);
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@CheckSecurity.Cidade.PodeEditar
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable Long id) {
		cidadeService.remover(id);
	}

	@CheckSecurity.Cidade.PodeEditar
	@PutMapping("/{id}")
	public CidadeModel atualizar(@PathVariable Long id, @Valid @RequestBody CidadeInput cidadeInput) {
		try {
			Cidade cidadeAtual = cidadeRepository.buscarOuFalhar(id);
			// BeanUtils.copyProperties(cidadeInput, cidadeAtual, "id");

			cidadeAtual.setEstado(new Estado());
			
			inputMapper.copyToDomainObject(cidadeInput, cidadeAtual);
			cidadeAtual = cidadeService.salvar(cidadeAtual);

			return modelMapper.toModel(cidadeAtual, CidadeModel.class);
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
