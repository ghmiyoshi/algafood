package com.algaworks.algafood.api.v1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.EstadoInput;
import com.algaworks.algafood.api.v1.model.model.EstadoModel;
import com.algaworks.algafood.api.v1.openapi.controller.EstadoControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.repository.EstadoRepository;
import com.algaworks.algafood.domain.service.CadastroEstadoService;

@RestController
@RequestMapping(path = "/v1/estados", produces = MediaType.APPLICATION_JSON_VALUE)
public class EstadoController implements EstadoControllerOpenApi{

	@Autowired
	private EstadoRepository estadoRepository;

	@Autowired
	private CadastroEstadoService estadoService;

	@Autowired
	private GenericModelMapper<Estado, EstadoModel> modelMapper;

	@Autowired
	private GenericInputMapper<EstadoInput, Estado> inputMapper;

	@CheckSecurity.Estados.PodeConsultar
	@GetMapping
	public List<EstadoModel> listar() {
		return modelMapper.toCollectionModel(estadoRepository.findAll(), EstadoModel.class);
	}

	@CheckSecurity.Estados.PodeConsultar
	@GetMapping("/{id}")
	public EstadoModel buscar(@PathVariable Long id) {
		return modelMapper.toModel(estadoRepository.buscarOuFalhar(id), EstadoModel.class);
	}

	@CheckSecurity.Estados.PodeEditar
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public EstadoModel adicionar(@Valid @RequestBody EstadoInput estadoInput) {
		Estado estado = inputMapper.toDomain(estadoInput, Estado.class);
		estado = estadoService.salvar(estado);

		return modelMapper.toModel(estado, EstadoModel.class);
	}

	@CheckSecurity.Estados.PodeEditar
	@PutMapping("/{id}")
	public EstadoModel atualizar(@PathVariable Long id, @Valid @RequestBody EstadoInput estadoInput) {
		Estado estadoAtual = estadoRepository.buscarOuFalhar(id);

		inputMapper.copyToDomainObject(estadoInput, estadoAtual);

		// BeanUtils.copyProperties(estado, estadoAtual, "id");
		estadoAtual = estadoService.salvar(estadoAtual);

		return modelMapper.toModel(estadoAtual, EstadoModel.class);
	}

	@CheckSecurity.Estados.PodeEditar
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		estadoService.excluir(id);
	}

}
