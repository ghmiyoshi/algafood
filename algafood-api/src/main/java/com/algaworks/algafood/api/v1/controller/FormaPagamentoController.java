
package com.algaworks.algafood.api.v1.controller;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.FormaPagamentoInput;
import com.algaworks.algafood.api.v1.model.model.FormaPagamentoModel;
import com.algaworks.algafood.api.v1.openapi.controller.FormaPagamentoControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.repository.FormaPagamentoRepository;
import com.algaworks.algafood.domain.service.CadastroFormaPagamentoService;

@RestController
@RequestMapping(path = "/v1/forma-pagamento", produces = MediaType.APPLICATION_JSON_VALUE)
public class FormaPagamentoController implements FormaPagamentoControllerOpenApi{

	@Autowired
	private CadastroFormaPagamentoService formaPagamentoService;

	@Autowired
	private FormaPagamentoRepository formaPagamentoRepository;

	@Autowired
	private GenericModelMapper<FormaPagamento, FormaPagamentoModel> modelMapper;

	@Autowired
	private GenericInputMapper<FormaPagamentoInput, FormaPagamento> inputMapper;

	@CheckSecurity.FormaPagamento.PodeConsultar
	@GetMapping
	public ResponseEntity<List<FormaPagamentoModel>> listar(ServletWebRequest request) {
		ShallowEtagHeaderFilter.disableContentCaching(request.getRequest()); // Desabilito o Shallow Etag do filter
		
		String eTag = "0"; // Valor inicial do eTag, como se fosse um Hash para saber se houve mudanças
		
		OffsetDateTime dataUltimaAtualizacao = formaPagamentoRepository.getUltimaAtualizacao();
		
		if(dataUltimaAtualizacao != null) {
			eTag = String.valueOf(dataUltimaAtualizacao.toEpochSecond()); // Pego numero de segundos e converto para String
		}
		
		// Tenho condição de saber se continua ou não o processamento
		if(request.checkNotModified(eTag)) {
			return null;
		}
		
		List<FormaPagamentoModel> formasPagamento = modelMapper.toCollectionModel(formaPagamentoRepository.findAll(), FormaPagamentoModel.class);
		return ResponseEntity.ok()
				.cacheControl(CacheControl.maxAge(10, TimeUnit.SECONDS).cachePublic())
				.eTag(eTag)
				.body(formasPagamento);
	}

	@CheckSecurity.FormaPagamento.PodeEditar
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public FormaPagamentoModel adicionar(@Valid @RequestBody FormaPagamentoInput formaPagamentoInput) {
		FormaPagamento formaPagamento = inputMapper.toDomain(formaPagamentoInput, FormaPagamento.class);
		formaPagamento = formaPagamentoService.salvar(formaPagamento);

		return modelMapper.toModel(formaPagamento, FormaPagamentoModel.class);
	}

	@CheckSecurity.FormaPagamento.PodeConsultar
	@GetMapping("/{id}")
	public ResponseEntity<FormaPagamentoModel> buscar(@PathVariable Long formaPagamentoId,
			ServletWebRequest request) {
		
		ShallowEtagHeaderFilter.disableContentCaching(request.getRequest());
		
		String eTag = "0";
		
		OffsetDateTime dataAtualizacao = formaPagamentoRepository
				.getDataAtualizacaoById(formaPagamentoId);
		
		if (dataAtualizacao != null) {
			eTag = String.valueOf(dataAtualizacao.toEpochSecond());
		}
		
		if (request.checkNotModified(eTag)) {
			return null;
		}
		
		FormaPagamento formaPagamento = formaPagamentoRepository.buscarOuFalhar(formaPagamentoId);
		
		FormaPagamentoModel formaPagamentoModel = modelMapper.toModel(formaPagamento, FormaPagamentoModel.class);
		
		return ResponseEntity.ok()
				.cacheControl(CacheControl.maxAge(10, TimeUnit.SECONDS))
				.eTag(eTag)
				.body(formaPagamentoModel);
	}

	@CheckSecurity.FormaPagamento.PodeEditar
	@PutMapping("/{id}")
	public FormaPagamentoModel atualizar(@PathVariable Long id, @Valid @RequestBody FormaPagamentoInput formaPagamentoInput) {
		FormaPagamento formaPagamento = formaPagamentoRepository.buscarOuFalhar(id);

		inputMapper.copyToDomainObject(formaPagamentoInput, formaPagamento);
		
		formaPagamento = formaPagamentoService.salvar(formaPagamento);
		
		return modelMapper.toModel(formaPagamento, FormaPagamentoModel.class);
	}
	
	@CheckSecurity.FormaPagamento.PodeEditar
	@DeleteMapping("/{id}")
	public void remover(@PathVariable Long id) {
		formaPagamentoService.excluir(id);
	}

}
