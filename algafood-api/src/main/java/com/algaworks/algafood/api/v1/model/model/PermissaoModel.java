package com.algaworks.algafood.api.v1.model.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PermissaoModel {

	@ApiModelProperty(example = "1", position = 1)
	private Long id;

	@ApiModelProperty(example = "CONSULTAR_COZINHAS", position = 2)
	private String nome;

	@ApiModelProperty(example = "Permite consultar cozinhas", position = 3)
	private String descricao;

}
