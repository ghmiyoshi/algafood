package com.algaworks.algafood.api.v1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.CozinhaInput;
import com.algaworks.algafood.api.v1.model.model.CozinhaModel;
import com.algaworks.algafood.api.v1.openapi.controller.CozinhaControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.core.web.AlgaFoodMediaTypes;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;

@RestController
@RequestMapping(path = "/v1/cozinhas", produces = AlgaFoodMediaTypes.V1_APPLICATION_JSON_VALUE)
public class CozinhaController implements CozinhaControllerOpenApi {

	@Autowired
	private CozinhaRepository cozinhaRepository;

	@Autowired
	private CadastroCozinhaService cozinhaService;

	@Autowired
	private GenericModelMapper<Cozinha, CozinhaModel> modelMapper;

	@Autowired
	private GenericInputMapper<CozinhaInput, Cozinha> inputMapper;

	@CheckSecurity.Cozinhas.PodeConsultar
	@GetMapping
	public Page<CozinhaModel> listar(@PageableDefault(size = 10)Pageable pageable) {
		System.out.println(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
		
		Page<Cozinha> cozinhasPage = cozinhaRepository.findAll(pageable);
		
		List<CozinhaModel> cozinhasModel = modelMapper.toCollectionModel(cozinhasPage.getContent(), CozinhaModel.class);
		
		Page<CozinhaModel> cozinhasModelPage = new PageImpl<>(cozinhasModel, pageable, cozinhasPage.getTotalElements());
		
		return cozinhasModelPage;
	}

	@CheckSecurity.Cozinhas.PodeConsultar
	@GetMapping("/{id}")
	public CozinhaModel buscar(@PathVariable Long id) {
		return modelMapper.toModel(cozinhaRepository.buscarOuFalhar(id), CozinhaModel.class);
	}

	@CheckSecurity.Cozinhas.PodeEditar
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CozinhaModel adicionar(@Valid @RequestBody CozinhaInput cozinhaInput) {
		Cozinha cozinha = inputMapper.toDomain(cozinhaInput, Cozinha.class);
		cozinha = cozinhaService.salvar(cozinha);

		return modelMapper.toModel(cozinha, CozinhaModel.class);
	}

	@CheckSecurity.Cozinhas.PodeEditar
	@PutMapping("/{id}")
	public CozinhaModel atualizar(@PathVariable Long id, @Valid @RequestBody CozinhaInput cozinhaInput) {
		Cozinha cozinhaAtual = cozinhaRepository.buscarOuFalhar(id);

		inputMapper.copyToDomainObject(cozinhaInput, cozinhaAtual);

		// BeanUtils.copyProperties(cozinhaInput, cozinhaAtual, "id");
		cozinhaAtual = cozinhaService.salvar(cozinhaAtual);

		return modelMapper.toModel(cozinhaAtual, CozinhaModel.class);
	}

	@CheckSecurity.Cozinhas.PodeEditar
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		cozinhaService.excluir(id);
	}

}
