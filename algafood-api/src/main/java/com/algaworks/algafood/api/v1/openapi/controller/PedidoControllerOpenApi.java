package com.algaworks.algafood.api.v1.openapi.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.RequestParam;

import com.algaworks.algafood.api.exception.ApiError;
import com.algaworks.algafood.api.v1.model.input.PedidoInput;
import com.algaworks.algafood.api.v1.model.model.PedidoModel;
import com.algaworks.algafood.api.v1.model.model.PedidoResumoModel;
import com.algaworks.algafood.domain.filter.PedidoFilter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Pedidos")
public interface PedidoControllerOpenApi {

	@ApiOperation("Pesquisa os pedidos")
	Page<PedidoResumoModel> pesquisar(Pageable pageable, PedidoFilter filtro);

	@ApiOperation("Registra um pedido")
	@ApiResponses({ 
			@ApiResponse(code = 201, message = "Pedido registrado"), })
	PedidoModel salvar(
			@ApiParam(name = "corpo", value = "Representação de um novo pedido") PedidoInput pedidoInput);

	@ApiImplicitParams({
			@ApiImplicitParam(value = "Nomes das propriedades para filtrar na resposta, separados por vírgula", name = "campos", paramType = "query", type = "string") })
	@ApiOperation("Busca um pedido por código")
	@ApiResponses({ 
			@ApiResponse(code = 404, message = "Pedido não encontrado", response = ApiError.class) })
	PedidoModel buscar(
			@ApiParam(value = "Código de um pedido", example = "f9981ca4-5a5e-4da3-af04-933861df3e55", required = true) String codigoPedido);
	
	@ApiImplicitParams({
		@ApiImplicitParam(value = "Nomes das propriedades para filtrar na resposta, separados por vírgula", name = "campos", paramType = "query", type = "string") })
	MappingJacksonValue listar(@RequestParam(required = false) String campos);
	
}
