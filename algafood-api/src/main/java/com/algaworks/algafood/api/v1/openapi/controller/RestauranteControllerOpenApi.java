package com.algaworks.algafood.api.v1.openapi.controller;

import java.util.List;

import org.springframework.http.converter.json.MappingJacksonValue;

import com.algaworks.algafood.api.exception.ApiError;
import com.algaworks.algafood.api.v1.model.input.RestauranteInput;
import com.algaworks.algafood.api.v1.model.model.RestauranteModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Restaurantes")
public interface RestauranteControllerOpenApi {
	
	@ApiOperation("Lista os restaurantes")
	MappingJacksonValue listar(
			@ApiParam(value = "Nome da projeção de pedidos", allowableValues = "apenas-nome, completo",
			type = "string", required = true) String projecao);

	@ApiOperation("Busca um restaurante por ID")
	@ApiResponses({ @ApiResponse(code = 400, message = "ID do restaurante inválido", response = ApiError.class),
			@ApiResponse(code = 404, message = "Restaurante não encontrado", response = ApiError.class) })
	RestauranteModel buscar(
			@ApiParam(value = "ID de um restaurante", example = "1", required = true) Long restauranteId);

	@ApiOperation("Cadastra um restaurante")
	@ApiResponses({ @ApiResponse(code = 201, message = "Restaurante cadastrado"), })
	RestauranteModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de um novo restaurante", required = true) RestauranteInput restauranteInput);

	@ApiOperation("Atualiza um restaurante por ID")
	@ApiResponses({ @ApiResponse(code = 200, message = "Restaurante atualizado"),
			@ApiResponse(code = 404, message = "Restaurante não encontrado", response = ApiError.class) })
	RestauranteModel atualizar(
			@ApiParam(value = "ID de um restaurante", example = "1", required = true) Long restauranteId,

			@ApiParam(name = "corpo", value = "Representação de um restaurante com os novos dados", required = true) RestauranteInput restauranteInput);

	@ApiOperation("Ativa um restaurante por ID")
	@ApiResponses({ @ApiResponse(code = 204, message = "Restaurante ativado com sucesso"),
			@ApiResponse(code = 404, message = "Restaurante não encontrado", response = ApiError.class) })
	void ativar(@ApiParam(value = "ID de um restaurante", example = "1", required = true) Long restauranteId);

	@ApiOperation("Inativa um restaurante por ID")
	@ApiResponses({ @ApiResponse(code = 204, message = "Restaurante inativado com sucesso"),
			@ApiResponse(code = 404, message = "Restaurante não encontrado", response = ApiError.class) })
	void inativar(@ApiParam(value = "ID de um restaurante", example = "1", required = true) Long restauranteId);

	@ApiOperation("Ativa múltiplos restaurantes")
	@ApiResponses({ @ApiResponse(code = 204, message = "Restaurantes ativados com sucesso") })
	void ativacoes(
			@ApiParam(name = "corpo", value = "IDs de restaurantes", required = true) List<Long> restauranteIds);

	@ApiOperation("Inativa múltiplos restaurantes")
	@ApiResponses({ @ApiResponse(code = 204, message = "Restaurantes ativados com sucesso") })
	void inativacoes(
			@ApiParam(name = "corpo", value = "IDs de restaurantes", required = true) List<Long> restauranteIds);

	@ApiOperation("Abre um restaurante por ID")
	@ApiResponses({ @ApiResponse(code = 204, message = "Restaurante aberto com sucesso"),
			@ApiResponse(code = 404, message = "Restaurante não encontrado", response = ApiError.class) })
	void abrir(@ApiParam(value = "ID de um restaurante", example = "1", required = true) Long restauranteId);

	@ApiOperation("Fecha um restaurante por ID")
	@ApiResponses({ @ApiResponse(code = 204, message = "Restaurante fechado com sucesso"),
			@ApiResponse(code = 404, message = "Restaurante não encontrado", response = ApiError.class) })
	void fechar(@ApiParam(value = "ID de um restaurante", example = "1", required = true) Long restauranteId);

}
