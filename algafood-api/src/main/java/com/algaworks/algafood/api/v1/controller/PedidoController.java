package com.algaworks.algafood.api.v1.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.PedidoInput;
import com.algaworks.algafood.api.v1.model.model.PedidoModel;
import com.algaworks.algafood.api.v1.model.model.PedidoResumoModel;
import com.algaworks.algafood.api.v1.openapi.controller.PedidoControllerOpenApi;
import com.algaworks.algafood.core.pageable.PageableTranslator;
import com.algaworks.algafood.core.security.AlgaSecurity;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.domain.exception.EntidadeNaoEncontradaException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.filter.PedidoFilter;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.PedidoRepository;
import com.algaworks.algafood.domain.service.EmissaoPedidoService;
import com.algaworks.algafood.infrastructure.repository.spec.PedidoSpecs;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
@RequestMapping(path = "/v1/pedidos", produces = MediaType.APPLICATION_JSON_VALUE)
public class PedidoController implements PedidoControllerOpenApi{

	@Autowired
	private PedidoRepository pedidoRepository;

	@Autowired
	private GenericModelMapper<Pedido, PedidoModel> modelMapper;

	@Autowired
	private GenericInputMapper<PedidoInput, Pedido> inputMapper;

	@Autowired
	private GenericModelMapper<Pedido, PedidoResumoModel> resumoModelMapper;

	@Autowired
	private EmissaoPedidoService pedidoService;
	
	@Autowired
	private AlgaSecurity algaSecurity;

	@CheckSecurity.Pedidos.PodeBuscar
	@GetMapping("/{codigoPedido}")
	public PedidoModel buscar(@PathVariable String codigoPedido) {
		Pedido pedido = pedidoService.buscarOuFalhar(codigoPedido);

		return modelMapper.toModel(pedido, PedidoModel.class);
	}

	@CheckSecurity.Pedidos.PodePesquisar
	@GetMapping
	public Page<PedidoResumoModel> pesquisar(@PageableDefault(size = 10) Pageable pageable, PedidoFilter filtro) {
		pageable = traduzirPageable(pageable);
		
		Page<Pedido> pedidos = pedidoRepository.findAll(PedidoSpecs.usandoFiltro(filtro), pageable);

		List<PedidoResumoModel> pedidosResumoModel = resumoModelMapper.toCollectionModel(pedidos.getContent(), PedidoResumoModel.class);
		
		Page<PedidoResumoModel> pedidosResumoModelPage = new PageImpl<>(pedidosResumoModel, pageable, pedidos.getTotalElements());
		
		return pedidosResumoModelPage;
	}
	
	private Pageable traduzirPageable(Pageable pageable) {
		var mapeamento = Map.of(
				"codigo", "codigo",
				"subtotal", "subtotal",
				"taxaFrete", "taxaFrete",
				"valorTotal", "valorTotal",
				"dataCriacao", "dataCriacao",
				"restaurante.nome", "restaurante.nome",
				"restaurante.id", "restaurante.id",
				"cliente.id", "cliente.id",
				"cliente.nome", "cliente.nome"
			);
		
		return PageableTranslator.translator(pageable, mapeamento);
	}
	
	/**
	 * Esse método funciona somente quando é definido @JsonFilter("pedidoFilter") no PedidoResumoModel
	 */
	@GetMapping("/campos")
	public MappingJacksonValue listar(@RequestParam(required = false) String campos) {
		List<Pedido> pedidos = pedidoRepository.findAll();
		List<PedidoResumoModel> pedidosModel = resumoModelMapper.toCollectionModel(pedidos, PedidoResumoModel.class);

		MappingJacksonValue pedidosWrapper = new MappingJacksonValue(pedidosModel);
		SimpleFilterProvider filterProvider = new SimpleFilterProvider();

		filterProvider.addFilter("pedidoFilter", SimpleBeanPropertyFilter.serializeAll()); // pedidoFilter definido na model PedidoResumoModel

		if (StringUtils.isNotBlank(campos)) {
			String[] camposArray = campos.split(",");

			validarCampos(camposArray);

			filterProvider.addFilter("pedidoFilter", SimpleBeanPropertyFilter.filterOutAllExcept(camposArray));
		}

		pedidosWrapper.setFilters(filterProvider);

		return pedidosWrapper;
	}

	private void validarCampos(String[] camposArray) {
		Set<String> camposInexistentes = new HashSet<>();

		for (String campo : camposArray) {
			if (ReflectionUtils.findField(PedidoResumoModel.class, campo) == null) {
				camposInexistentes.add(campo);
			}
		}

		if (!camposInexistentes.isEmpty()) {
			throw new NegocioException(String.format("O campo %s não existe no modelo.", camposInexistentes));
		}
	}

	@CheckSecurity.Pedidos.PodeCriar
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public PedidoModel salvar(@RequestBody @Valid PedidoInput pedidoInput) {
		try {
			Pedido pedido = inputMapper.toDomain(pedidoInput, Pedido.class);

			pedido.setCliente(new Usuario());
			pedido.getCliente().setId(algaSecurity.getUsuarioId());

			pedido = pedidoService.emitir(pedido);

			return modelMapper.toModel(pedido, PedidoModel.class);
		} catch (EntidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
