package com.algaworks.algafood.api.v1.model.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

//@ApiModel(value = "Cidade", description = "Representação de uma cidade")
@Getter
@Setter
public class CidadeModel {

	@ApiModelProperty(name = "ID de uma cidade", example = "1")
	private Long id;

	@ApiModelProperty(name = "Nome de uma cidade", example = "Uberlândia")
	private String nome;

	private EstadoModel estado;

}
