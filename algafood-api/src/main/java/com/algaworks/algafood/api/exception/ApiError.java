package com.algaworks.algafood.api.exception;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@ApiModel("Erro")
@Getter
@Builder
@JsonInclude(Include.NON_NULL)
public class ApiError {

	@ApiModelProperty(example = "400", position = 1)
	// Implementação do Problem Details for HTTP APIs (RFC 7807)
	private Integer status;

	@ApiModelProperty(example = "2020-08-18T23:29:47.913608Z", position = 2)
	@Builder.Default
	private OffsetDateTime timestamp = OffsetDateTime.now();

	@ApiModelProperty(example = "https://algafood.com.br/recurso-nao-encontrado", position = 3)
	private String type;

	@ApiModelProperty(example = "Recurso não encontrado", position = 4)
	private String title;

	@ApiModelProperty(example = "Não existe um cadastro de Cozinha com código 14", position = 5)
	private String detail;

	@ApiModelProperty(example = "Não existe um cadastro de Cozinha com código 14", position = 6)
	private String userMessage;

	@ApiModelProperty(value = "Lista de objetos ou campos que geraram o erro (opcional)", position = 7)
	private List<Object> objects;

	@ApiModel("ObjetoErro")
	@Getter
	@Builder
	public static class Object {

		private String name;

		private String userMessage;

	}

}
