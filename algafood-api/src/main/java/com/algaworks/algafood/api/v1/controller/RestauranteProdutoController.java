package com.algaworks.algafood.api.v1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.ProdutoInput;
import com.algaworks.algafood.api.v1.model.model.ProdutoModel;
import com.algaworks.algafood.api.v1.openapi.controller.RestauranteProdutoControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.ProdutoRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.domain.service.CadastroProdutoService;

@RestController
@RequestMapping(path = "/v1/{restauranteId}/produtos", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestauranteProdutoController implements RestauranteProdutoControllerOpenApi{

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private RestauranteRepository restauranteRepository;

	@Autowired
	private CadastroProdutoService produtoService;

	@Autowired
	private GenericModelMapper<Produto, ProdutoModel> modelMapper;

	@Autowired
	private GenericInputMapper<ProdutoInput, Produto> inputMapper;

	@CheckSecurity.Restaurantes.PodeConsultar
	@GetMapping
	public List<ProdutoModel> listar(@PathVariable Long restauranteId, @RequestParam(required = false) Boolean ativo) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);
		List<Produto> todosProdutos = produtoRepository.findByRestaurante(restaurante);
		
		if(ativo != null) {
			todosProdutos = produtoRepository.findAllProdutosAtivos(restauranteId, ativo);
		}

		return modelMapper.toCollectionModel(todosProdutos, ProdutoModel.class);
	}

	@CheckSecurity.Restaurantes.PodeConsultar
	@GetMapping("/{produtoId}")
	public ProdutoModel buscar(@PathVariable Long restauranteId, @PathVariable Long produtoId) {
		Produto produto = produtoService.buscarOuFalhar(restauranteId, produtoId);

		return modelMapper.toModel(produto, ProdutoModel.class);
	}

	@CheckSecurity.Restaurantes.PodeGerenciarCadastro
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ProdutoModel adicionar(@PathVariable Long restauranteId, @RequestBody @Valid ProdutoInput produtoInput) {
		Restaurante restaurante = restauranteRepository.buscarOuFalhar(restauranteId);

		Produto produto = inputMapper.toDomain(produtoInput, Produto.class);
		produto.setRestaurante(restaurante);

		produto = produtoService.salvar(produto);

		return modelMapper.toModel(produto, ProdutoModel.class);
	}

	@CheckSecurity.Restaurantes.PodeGerenciarCadastro
	@PutMapping("/{produtoId}")
	public ProdutoModel atualizar(@PathVariable Long restauranteId, @PathVariable Long produtoId, @RequestBody @Valid ProdutoInput produtoInput) {
		Produto produtoAtual = produtoService.buscarOuFalhar(restauranteId, produtoId);

		inputMapper.copyToDomainObject(produtoInput, produtoAtual);

		produtoAtual = produtoService.salvar(produtoAtual);

		return modelMapper.toModel(produtoAtual, ProdutoModel.class);
	}
	
}
