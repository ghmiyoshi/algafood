package com.algaworks.algafood.api.v1.openapi.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.algaworks.algafood.api.exception.ApiError;
import com.algaworks.algafood.api.v1.model.input.SenhaInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioInputAtualizar;
import com.algaworks.algafood.api.v1.model.model.UsuarioModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Usuários")
public interface UsuarioControllerOpenApi {

	@ApiOperation("Lista os usuários")
	List<UsuarioModel> listar();

	@ApiOperation("Busca um usuário por ID")
	@ApiResponses({ @ApiResponse(code = 400, message = "ID do usuário inválido", response = ApiError.class),
			@ApiResponse(code = 404, message = "Usuário não encontrado", response = ApiError.class) })
	UsuarioModel buscar(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId);

	@ApiOperation("Cadastra um usuário")
	@ApiResponses({ @ApiResponse(code = 201, message = "Usuário cadastrado"), })
	UsuarioModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de um novo usuário", required = true) UsuarioInput usuarioInput);

	@ApiOperation("Atualiza um usuário por ID")
	@ApiResponses({ @ApiResponse(code = 200, message = "Usuário atualizado"),
			@ApiResponse(code = 404, message = "Usuário não encontrado", response = ApiError.class) })
	UsuarioModel atualizar(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId,

			@ApiParam(name = "corpo", value = "Representação de um usuário com os novos dados", required = true) UsuarioInputAtualizar usuarioInputAtualizar);

	@ApiOperation("Atualiza a senha de um usuário")
	@ApiResponses({ @ApiResponse(code = 204, message = "Senha alterada com sucesso"),
			@ApiResponse(code = 404, message = "Usuário não encontrado", response = ApiError.class) })
	void alterarSenha(@ApiParam(value = "ID do usuário", example = "1", required = true) Long usuarioId,

			@ApiParam(name = "corpo", value = "Representação de uma nova senha", required = true) SenhaInput senha);
	
	@ApiOperation("Exclui um usuário por ID")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Usuário excluído", response = ApiError.class),
		@ApiResponse(code = 404, message = "Usuário não encontrado", response = ApiError.class)
	})
	void delete(@PathVariable Long id);
	
}
