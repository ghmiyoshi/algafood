package com.algaworks.algafood.api.v2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "CidadeModel", description = "Representação de uma cidade")
@Getter
@Setter
public class CidadeV2Model {

	@ApiModelProperty(name = "ID de uma cidade", example = "1")
	private Long idCidade;

	@ApiModelProperty(name = "Nome de uma cidade", example = "Uberlândia")
	private String nomeCidade;

	private Long idEstado;

	private String nomeEstado;

}
