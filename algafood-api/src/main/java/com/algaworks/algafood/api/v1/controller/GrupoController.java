package com.algaworks.algafood.api.v1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.GrupoInput;
import com.algaworks.algafood.api.v1.model.model.GrupoModel;
import com.algaworks.algafood.api.v1.openapi.controller.GrupoControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.domain.model.Grupo;
import com.algaworks.algafood.domain.repository.GrupoRepository;
import com.algaworks.algafood.domain.service.CadastroGrupoService;

@RestController
@RequestMapping(path = "/v1/grupos", produces = MediaType.APPLICATION_JSON_VALUE)
public class GrupoController implements GrupoControllerOpenApi {

	@Autowired
	private CadastroGrupoService cadastroGrupoService;

	@Autowired
	private GrupoRepository grupoRepository;

	@Autowired
	private GenericModelMapper<Grupo, GrupoModel> modelMapper;

	@Autowired
	private GenericInputMapper<GrupoInput, Grupo> inputMapper;

	@CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
	@GetMapping
	public List<GrupoModel> listar() {
		List<Grupo> grupos = grupoRepository.findAll();

		return modelMapper.toCollectionModel(grupos, GrupoModel.class);
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
	@GetMapping("/{id}")
	public GrupoModel buscar(@PathVariable Long id) {
		Grupo grupo = grupoRepository.buscarOuFalhar(id);

		return modelMapper.toModel(grupo, GrupoModel.class);
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeEditar
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public GrupoModel adicionar(@Valid @RequestBody GrupoInput grupoInput) {
		Grupo grupo = inputMapper.toDomain(grupoInput, Grupo.class);

		grupo = cadastroGrupoService.adicionar(grupo);

		return modelMapper.toModel(grupo, GrupoModel.class);
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeEditar
	@PutMapping("/{id}")
	public GrupoModel alterar(@PathVariable Long id, @Valid @RequestBody GrupoInput grupoInput) {
		Grupo grupo = grupoRepository.buscarOuFalhar(id);

		inputMapper.copyToDomainObject(grupoInput, grupo);

		grupo = cadastroGrupoService.adicionar(grupo);

		return modelMapper.toModel(grupo, GrupoModel.class);
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeEditar
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		cadastroGrupoService.excluir(id);
	}

}
