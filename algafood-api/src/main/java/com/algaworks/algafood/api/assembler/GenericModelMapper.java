package com.algaworks.algafood.api.assembler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericModelMapper<T, S> {

	@Autowired
	private ModelMapper modelMapper;

	public S toModel(T domainObject, Class<S> type) {
		return modelMapper.map(domainObject, type);
	}

	public List<S> toCollectionModel(Collection<T> objects, Class<S> type) {
		return objects.stream().map(object -> toModel(object, type)).collect(Collectors.toList());
	}

}
