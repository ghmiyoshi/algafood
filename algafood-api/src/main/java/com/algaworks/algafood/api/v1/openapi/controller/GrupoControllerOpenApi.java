package com.algaworks.algafood.api.v1.openapi.controller;

import java.util.List;

import com.algaworks.algafood.api.exception.ApiError;
import com.algaworks.algafood.api.v1.model.input.GrupoInput;
import com.algaworks.algafood.api.v1.model.model.GrupoModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Grupos")
public interface GrupoControllerOpenApi {

	@ApiOperation("Lista os grupos")
	List<GrupoModel> listar();

	@ApiOperation("Busca um grupo por ID")
	@ApiResponses({ 
			@ApiResponse(code = 400, message = "ID da grupo inválido", response = ApiError.class),
			@ApiResponse(code = 404, message = "Grupo não encontrado", response = ApiError.class) })
	GrupoModel buscar(
			@ApiParam(value = "ID de um grupo", example = "1", required = true) Long grupoId);

	@ApiOperation("Cadastra um grupo")
	@ApiResponses({ 
			@ApiResponse(code = 201, message = "Grupo cadastrado"), })
	GrupoModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de um novo grupo") GrupoInput grupoInput);

	@ApiOperation("Atualiza um grupo por ID")
	@ApiResponses({ 
			@ApiResponse(code = 200, message = "Grupo atualizado"),
			@ApiResponse(code = 404, message = "Grupo não encontrado", response = ApiError.class) })
	GrupoModel alterar(
			@ApiParam(value = "ID de um grupo", example = "1", required = true) Long grupoId,
			@ApiParam(name = "corpo", value = "Representação de um grupo com os novos dados") GrupoInput grupoInput);

	@ApiOperation("Exclui um grupo por ID")
	@ApiResponses({ 
			@ApiResponse(code = 204, message = "Grupo excluído"),
			@ApiResponse(code = 404, message = "Grupo não encontrado", response = ApiError.class) })
	void remover(
			@ApiParam(value = "ID de um grupo", example = "1", required = true) Long grupoId);

}
