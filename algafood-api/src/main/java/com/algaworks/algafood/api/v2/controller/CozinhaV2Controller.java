package com.algaworks.algafood.api.v2.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v2.model.CozinhaV2Model;
import com.algaworks.algafood.api.v2.model.input.CozinhaV2Input;
import com.algaworks.algafood.api.v2.openapi.controller.CozinhaV2ControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.core.web.AlgaFoodMediaTypes;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;

import io.swagger.annotations.Api;

@Api(tags = "Cozinhas")
@RestController
@RequestMapping(path = "/v2/cozinhas", produces = AlgaFoodMediaTypes.V2_APPLICATION_JSON_VALUE)
public class CozinhaV2Controller implements CozinhaV2ControllerOpenApi {

	@Autowired
	private CozinhaRepository cozinhaRepository;

	@Autowired
	private CadastroCozinhaService cozinhaService;

	@Autowired
	private GenericModelMapper<Cozinha, CozinhaV2Model> modelMapper;

	@Autowired
	private GenericInputMapper<CozinhaV2Input, Cozinha> inputMapper;

	@CheckSecurity.Cozinhas.PodeConsultar
	@GetMapping
	public Page<CozinhaV2Model> listar(@PageableDefault(size = 10)Pageable pageable) {
		Page<Cozinha> cozinhasPage = cozinhaRepository.findAll(pageable);
		
		List<CozinhaV2Model> cozinhasModel = modelMapper.toCollectionModel(cozinhasPage.getContent(), CozinhaV2Model.class);
		
		Page<CozinhaV2Model> cozinhasModelPage = new PageImpl<>(cozinhasModel, pageable, cozinhasPage.getTotalElements());
		
		return cozinhasModelPage;
	}

	@CheckSecurity.Cozinhas.PodeConsultar
	@GetMapping("/{id}")
	public CozinhaV2Model buscar(@PathVariable Long id) {
		return modelMapper.toModel(cozinhaRepository.buscarOuFalhar(id), CozinhaV2Model.class);
	}

	@CheckSecurity.Cozinhas.PodeEditar
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CozinhaV2Model adicionar(@Valid @RequestBody CozinhaV2Input cozinhaV2Input) {
		cozinhaService.validaNome(cozinhaV2Input.getNomeCozinha());
		
		Cozinha cozinha = inputMapper.toDomain(cozinhaV2Input, Cozinha.class);
		cozinha = cozinhaService.salvar(cozinha);

		return modelMapper.toModel(cozinha, CozinhaV2Model.class);
	}

	@CheckSecurity.Cozinhas.PodeEditar
	@PutMapping("/{id}")
	public CozinhaV2Model atualizar(@PathVariable Long id, @Valid @RequestBody CozinhaV2Input CozinhaV2Input) {
		Cozinha cozinhaAtual = cozinhaRepository.buscarOuFalhar(id);

		inputMapper.copyToDomainObject(CozinhaV2Input, cozinhaAtual);

		// BeanUtils.copyProperties(CozinhaV2Input, cozinhaAtual, "id");
		cozinhaAtual = cozinhaService.salvar(cozinhaAtual);

		return modelMapper.toModel(cozinhaAtual, CozinhaV2Model.class);
	}

	@CheckSecurity.Cozinhas.PodeEditar
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		cozinhaService.excluir(id);
	}

}
