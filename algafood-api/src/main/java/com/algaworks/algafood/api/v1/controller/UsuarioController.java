package com.algaworks.algafood.api.v1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.GenericInputMapper;
import com.algaworks.algafood.api.assembler.GenericModelMapper;
import com.algaworks.algafood.api.v1.model.input.SenhaInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioInput;
import com.algaworks.algafood.api.v1.model.input.UsuarioInputAtualizar;
import com.algaworks.algafood.api.v1.model.model.UsuarioModel;
import com.algaworks.algafood.api.v1.openapi.controller.UsuarioControllerOpenApi;
import com.algaworks.algafood.core.security.annotation.CheckSecurity;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.UsuarioRepository;
import com.algaworks.algafood.domain.service.CadastroUsuarioService;

import io.swagger.annotations.Api;

@Api(tags = "Usuários")
@RestController
@RequestMapping(path = "/v1/usuarios", produces = MediaType.APPLICATION_JSON_VALUE)
public class UsuarioController implements UsuarioControllerOpenApi{

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private CadastroUsuarioService usuarioService;

	@Autowired
	private GenericModelMapper<Usuario, UsuarioModel> modelMapper;

	@Autowired
	private GenericInputMapper<UsuarioInput, Usuario> inputMapper;

	@Autowired
	private GenericInputMapper<UsuarioInputAtualizar, Usuario> inputAtualizarMapper;

	@CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
	@GetMapping
	public List<UsuarioModel> listar() {
		return modelMapper.toCollectionModel(usuarioRepository.findAll(), UsuarioModel.class);
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeConsultar
	@GetMapping("/{id}")
	public UsuarioModel buscar(@PathVariable Long id) {
		Usuario usuario = usuarioRepository.buscarOuFalhar(id);

		return modelMapper.toModel(usuario, UsuarioModel.class);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public UsuarioModel adicionar(@Valid @RequestBody UsuarioInput usuarioInput) {
		Usuario usuario = inputMapper.toDomain(usuarioInput, Usuario.class);
		usuario = usuarioService.salvar(usuario);

		return modelMapper.toModel(usuario, UsuarioModel.class);
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeAlterarUsuario
	@PutMapping("/{id}")
	public UsuarioModel atualizar(@PathVariable Long id, @Valid @RequestBody UsuarioInputAtualizar usuarioInputAtualizar) {
		Usuario usuarioAtual = usuarioRepository.buscarOuFalhar(id);

		inputAtualizarMapper.copyToDomainObject(usuarioInputAtualizar, usuarioAtual);
		usuarioAtual = usuarioService.salvar(usuarioAtual);

		return modelMapper.toModel(usuarioAtual, UsuarioModel.class);
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeAlterarPropriaSenha
	@PutMapping("/{id}/senha")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void alterarSenha(@PathVariable Long id, @Valid @RequestBody SenhaInput senhaInput) {
		usuarioService.alterarSenha(id, senhaInput.getSenhaAtual(), senhaInput.getNovaSenha());
	}

	@CheckSecurity.UsuariosGruposPermissoes.PodeEditar
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		usuarioService.excluir(id);
	}

}
