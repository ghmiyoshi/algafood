ALTER TABLE forma_pagamento ADD data_atualizacao datetime NULL;
UPDATE forma_pagamento set data_atualizacao = utc_timestamp;
ALTER TABLE forma_pagamento MODIFY data_atualizacao datetime NOT NULL;
